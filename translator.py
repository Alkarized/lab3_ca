import logging

from isa import write_code, Command, AddrMode, WORD_LENGTH, OPERAND_LENGTH

logging.basicConfig(level=logging.DEBUG)

text_command = {
    "LD": Command.LD,
    "SV": Command.SV,
    "INC": Command.INC,
    "DEC": Command.DEC,
    "ADD": Command.ADD,
    "SUB": Command.SUB,
    "MUL": Command.MUL,
    "DIV": Command.DIV,
    "IN": Command.IN,
    "OUT": Command.OUT,
    "CMP": Command.CMP,
    "JMP": Command.JMP,
    "JE": Command.JE,
    "JGE": Command.JGE,
    "JC": Command.JC,
    "HALT": Command.HALT,

}


def gel_all_labels(code: str) -> dict:
    operation_counter: int = 0
    code_segment: bool = False
    labels: dict = {}

    for i, line in enumerate(code.split("\n")):
        words: list[str] = line.split()
        if len(words) == 0:
            continue
        if words[0] == '.code':
            code_segment = True  # Показываем, что у нас есть сегмент кода.
            continue
        if code_segment:
            if words[0] == '' or words[0][0] == '#':  # Тут начинается комментарий либо пустая строка
                continue
            if words[0][-1] == ':':  # В конце первого слова есть ':' - значит это метка
                label_name: str = words[0]  # Имя метки
                label_name = label_name[0:len(label_name) - 1]
                assert label_name not in labels, f"Label {label_name} Метка уже существует"
                labels[label_name]: int = operation_counter
            elif len(words) > 0 and words[0].upper() in text_command:
                operation_counter += 1
            else:
                raise Exception(f'Ошибка в строчке ({i + 1})')
        else:
            continue
    return labels


def translate(text):
    terms: list = []
    labels: dict = {}
    variables: dict = {}
    data: list = []
    log_mem: list = []

    was_data_seg: bool = False
    was_code_seg: bool = False

    labels = gel_all_labels(text)

    assert ".start" in labels, "Нет метки .start!"

    for line in text.split("\n"):
        words_none_comments: list[str] = line.split("#")[0].split()
        if len(words_none_comments) == 0:
            continue

        if len(words_none_comments) == 1 and words_none_comments[0] == ".data":
            assert not was_data_seg, "Сегмент с данными уже был!"
            was_data_seg = True
        if len(words_none_comments) == 1 and words_none_comments[0] == ".code":
            assert not was_code_seg, "Сегмент с кодом уже был!"
            was_code_seg = True
            was_data_seg = False

        if was_data_seg:
            if words_none_comments[0] == ".data":
                continue

            assert len(words_none_comments) == 2, "Неправильное число параметров данных, нужно: var_name var_data"
            var_name: str = words_none_comments[0]
            assert var_name not in variables, \
                f"Variable with name {var_name} already defined"
            try:
                var_value: int = ord(words_none_comments[1])
            except TypeError:
                var_value: int = int(words_none_comments[1])

            assert var_value.bit_length() < WORD_LENGTH, "Большое число - не помещается значение переменной"

            data.append(var_value)
            variables[var_name] = len(data) - 1

        if was_code_seg:
            if words_none_comments[0] == ".code" or words_none_comments[0][-1] == ":":
                continue

            assert len(words_none_comments) < 3, "Неправильное число параметров кода, нужно: command arg если есть"

            mnemonic: str = words_none_comments[0].upper()

            if text_command[mnemonic].need_argument:
                assert len(words_none_comments) > 1, f"У команды {mnemonic} должен быть аргумент!"

                argument: str = words_none_comments[1]

                if argument[0] == "$":
                    straight: int = int(argument[1:])

                    assert straight.bit_length() < OPERAND_LENGTH, \
                        "Большое число - не помещается как аргумент к прямой адресации"

                    terms.append(text_command[mnemonic].code + straight + AddrMode.STRAIGHT)

                elif argument[0] == "^":
                    variable: str = argument[1:]
                    assert variable in variables, f"Нет такой переменной: {variable}"

                    terms.append(text_command[mnemonic].code + variables[variable] + 1 + AddrMode.ABSOLUTE)
                    # 1ка тут - это доп смещение от jmp вначале

                else:
                    assert argument in labels, f"Нет такой метки: {argument}"

                    terms.append(text_command[mnemonic].code + len(data) + 1 + labels[argument] + AddrMode.ABSOLUTE)
                    # 1ка тут - это доп смещение от jmp вначале

                log_mem.append(mnemonic)
            else:
                terms.append(text_command[mnemonic].code)
                log_mem.append(mnemonic)

    terms = data + terms
    terms = [text_command["JMP"].code + AddrMode.STRAIGHT + 1 + len(data) + labels[".start"]] + terms

    return terms, log_mem


def main(args):
    assert len(args) == 2, \
        "Wrong arguments: translator.py <input_file> <target_file>"

    source, target = args

    with open(source, "rt", encoding="utf-8") as f:
        source = f.read()

    code, log_mem = translate(source)

    for line in log_mem:
        logging.info(line)

    # print("source LoC:", len(source.split()), "code instr:", len(code))

    write_code(target, code)


if __name__ == '__main__':
    # main(sys.argv[1:])
    main(["tests/cat_test/input.asm", "tests/cat_test/result"])
