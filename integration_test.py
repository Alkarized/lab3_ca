# pylint: disable=missing-class-docstring     # чтобы не быть Капитаном Очевидностью
# pylint: disable=missing-function-docstring  # чтобы не быть Капитаном Очевидностью
# pylint: disable=line-too-long               # строки с ожидаемым выводом

"""Интеграционные тесты транслятора и машины
"""

import contextlib
import io
import os
import tempfile
import unittest

import pytest

import machine
import translator


@pytest.mark.golden_test("golden/*.yml")
def test_whole_by_golden(golden):
    with tempfile.TemporaryDirectory() as tmpdirname:
        source = os.path.join(tmpdirname, "source.asm")
        input_stream = os.path.join(tmpdirname, "input")
        target = os.path.join(tmpdirname, "target")

        with open(source, "w", encoding="utf-8") as file:
            file.write(golden["source"])
        with open(input_stream, "w", encoding="utf-8") as file:
            file.write(golden["input"])

        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            translator.main([source, target])
            print("============================================================")
            machine.main([target, input_stream])

        with open(target, encoding="utf-8") as file:
            code = file.read()

        assert code == golden.out["code"]
        assert stdout.getvalue() == golden.out["output"]


class TestTranslator(unittest.TestCase):

    def test_hello(self):
        # Создаём временную папку для скомпилированного файла. Удаляется автоматически.
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "tests/hello_test/input.asm"
            target = os.path.join(tmpdirname, "hello")
            input_stream = "tests/hello_test/input.txt"

            # Собираем весь стандартный вывод в переменную stdout.
            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])

            # Проверяем, что было напечатано то, что мы ожидали.
            self.assertEqual(stdout.getvalue(), 'Output:\nhello world!\ninstr_counter: 26 ticks: 77\n')

    def test_cat(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "tests/cat_test/input.asm"
            target = os.path.join(tmpdirname, "cat")
            input_stream = "tests/cat_test/input.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])

            self.assertEqual(stdout.getvalue(), 'Output:\ntest cat\ninstr_counter: 45 ticks: 134\n')

    def test_prob1(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "tests/prob1/input.asm"
            target = os.path.join(tmpdirname, "prob1")
            input_stream = "tests/prob1/input.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])

            self.assertEqual(stdout.getvalue(), 'Output:\n233168\ninstr_counter: 33 ticks: 112\n')
