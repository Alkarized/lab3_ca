# Лабораторная работа №3. На кончиках пальцев

Цель

- экспериментальное знакомство с устройством процессоров через моделирование; 
- получение опыта работы с компьютерной системой на нескольких уровнях организации.

Данная лабораторная работа носит практический характер. Она включает разработку:

- языка программирования и транслятора;
- системы команд;
- модели процессора;
- реализацию и тестирование работы алгоритма.

### Выполнил
ФИО: Оскилко Михаил Васильевич <br>
Группа: P33102

### Вариант

`asm | acc | neum | hw | instr | binary | stream | port | prob1`

### Язык программирования. Синтаксис

### `asm`
- синтаксис ассемблера. 
- Необходима поддержка label-ов.

### Архитектура
### `acc`

- система команд должна быть выстроена вокруг аккумулятора.

### Архитектура организации памяти
### `neum`

- фон Неймановская архитектура.

### Control Unit
### `hw`

- hardwired. Реализуется как часть модели.

### Точность модели
### `instr`

- процессор необходимо моделировать с точностью до каждой инструкции (наблюдается состояние после каждой инструкции).

### Представление машинного кода
### `binary`

- бинарное представление. При этом необходимо сделать экспорт в формате с мнемоникой команд для возможности анализа машинного кода.

### Ввод-вывод
#### `stream`

Ввод-вывод осуществляется как поток токенов. Есть в примере. Логика работы:

- при старте модели у вас есть буфер, в котором представлены все данные ввода (`['h', 'e', 'l', 'l', 'o']`);
- при обращении к вводу (выполнение инструкции), модель процессора получает "токен" (символ) информации;
- если данные в буфере кончились -- останавливайте моделирование;
- вывод данных реализуется аналогично, по выполнении команд в буфер вывода добавляется ещё один символ;
- по окончании моделирования показать все выведенные данные;
- логика работы с буфером реализуется в рамках модели на Python.

### Ввод-вывод ISA
### `port`

Поддержка ввода-вывода с точки зрения системы команд.

- port-mapped

### Алгоритм

- Входные данные должны подаваться через ввод.
- Результат должен быть подан на вывод.
- Формат ввода/вывода данных -- на ваше усмотрение.

#### prob1. Multiples of 3 or 5

[Project Euler. Problem 1](https://projecteuler.net/problem=1)

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.

## Описание ЯП.

- Язык поддерживет метки `label:` То есть можно написать метку и по ней обратиться потом.
- Так же в ЯП есть комментарии, `#` - означанает начало комментария и до конца строки
```
# Тут комментарий
command arg # Вот тут теперь комментарий!
```
- Есть необходимые метки вида `.data`, `.code`, `.start:`
  - `.data` Показывает, где находятся данные (переменные)
  - `.code` Исполняемый код
  - `.data` и `.code` обязаны находится на отдельной строчке
  - `.start:` Обязательная метка, где начинается исполнение кода
  - Сначала должен идти сегмент `.data`, а потом уже `.code`
- Любая метка обязана находится на пустой строчке - и быть первой!
- Данные в `.data` представлены в таком виде:
```
    ...
    .data
        my_var int_number # test 123
    ...
```
- Табуляция необязательна! (Используется для удобства написания кода).
- Где `my_var` - это объявление названия переменной, а `int_nubmer` - значение переменной.
- Для переменных использовать `^` в начале, для `label` такого не нада
- Тут надо понимать, что у нас все есть цифорки, поэтому смысла вводить кроме как для удобства `char` типы не имеет смысла.
- Адресация поддерживается по меткам, а так же существует возможность прямой загрузки числа (Для этого использовать `$` перед числом)
- Код выполняется последовательно, кроме использования операций ветвления.

### Код команд ЯП

| Синтаксис | Операнд            | Количество тактов          | Описание                                                                                    |
|:----------|:-------------------|:---------------------------|:--------------------------------------------------------------------------------------------|
| `LD M`    | Переменная / Число | 1 или 2                    | Загрузить значение в аккумулятор из памяти                                                  |     
| `SV M`    | Переменная         | 2                          | Выгрузить значение аккумулятора в память                                                    |   
| `INC`     | Ничего             | 1                          | Увеличить значение аккумулятора на 1                                                        |  
| `DEC`     | Ничего             | 1                          | Уменьшить значение аккумулятора на 1                                                        |  
| `ADD M`   | Переменная / Число | 1 или 2                    | Прибавить к аккумулятору число                                                              |     
| `SUB M`   | Переменная / Число | 1 или 2                    | Уменьшить значение аккумулятора на число                                                    |     
| `MUL M`   | Переменная / Число | 1 или 2                    | Умножить значение аккумулятора на число                                                     |     
| `DIV M`   | Переменная / Число | 1 или 2                    | Поделить значение аккумулятора на число                                                     |  
| `IN`      | Ничего             | 1                          | Ввод данных в аккумулятор                                                                   |
| `OUT`     | Ничего             | 1                          | Вывод данных из аккумулятора                                                                |
| `CMP M`   | Переменная / Число | 1 или 2                    | Сравнить число и аккумулятор с выставлением флагов NZC, без изменения значения аккумулятора | 
| `JMP M`   | Метка              | 2                          | Перейти на другой адрес                                                                     | 
| `JE M`    | Метка              | 2                          | Перейти если выставлен флаг Z (Ноль)                                                        | 
| `JGE M`   | Метка              | 2                          | Перейти если не выставлен флаг N (Отрицательное число)                                      | 
| `JC M`    | Метка              | 2                          | Перейти если выставлен флаг C - переполнения                                                | 
| `HALT`    | Ничего             | 2                          | Остановка                                                                                   | 

### Организация памяти

- Память инструкций и данных совмещена (Привет Нейману)
- Машинное слово - 32 бит, знаковое
- В силу того, что программу пишет человек, а архитектура памяти, говорит что данные и инструкции одинаковые, процессор их не распознает сам, то каких-то разделений нет. (Пояснение для себя)

**Memory**
``` 
+------------------------------+
| 00  : jmp n                  |
| 01  : variable               | 
| 02  : variable               |
|             ...              |
| k   : instruction            |
|             ...              |
| n   : program start          |
|             ...              |
| i   : instruction            |
| i+1 : instruction            |
|             ...              |
+------------------------------+
```
### Система команд

- Ввод-вывод осуществляется через порты, считывая и записывая данные на аккумулятор
- Изменение данных осуществляется через АЛУ и аккумулятор AC
- Инструкции те же, что и в ЯП, только идут в преобразованном виде (битовом)

| Мнемоника | Двоичный Код | 
|:----------|:-------------|
| `LD M`    | 0101         |    
| `SV M`    | 0110         |    
| `INC`     | 0001         |  
| `DEC`     | 0010         |   
| `ADD M`   | 0111         |      
| `SUB M`   | 1000         |      
| `MUL M`   | 1001         |      
| `DIV M`   | 1010         |  
| `IN`      | 0011         | 
| `OUT`     | 0100         | 
| `CMP M`   | 1011         | 
| `JMP M`   | 1100         | 
| `JE M`    | 1101         |  
| `JGE M`   | 1110         | 
| `JC M`    | 1111         | 
| `HALT`    | 0000         | 

### Структура машинного слова


- Первые 4 бита - `ins. code` - код инструкции (Табличка выше)
- Следующий 1 бит - `adr mode` - режим адресации (0 - прямая загрузка операнда или 1 - абсолютная)
- Оставшиеся 27 битов - `operand` – операнд комманды

### Транслятор

- Интерфейс командной строки: 
    `translator.py <input_file> <output_file>`

- Реализовано в модуле: [translator](translator.py)


**Особенности работы**

- Составление мапы меток и адресов
- Комментарии игнорируются
- Команды переводятся в инструкции в бинаром виде
- Проверка наличия и единственности `.start:`

### Процессор:

![alt text](img.png)

### Апробация

- Тесты лежат в папочке `test`
- А так же в файлике [integration_test](integration_test.py)

**Пример cat_test**

- cat.asm

```

.code
    .start:
        IN
        CMP $0
        JE .end
        OUT
        JMP .start
    .end:
        HALT


```

- translator

```

INFO:root:IN
INFO:root:CMP
INFO:root:JE
INFO:root:OUT
INFO:root:JMP
INFO:root:HALT

```
- translated code

```11000000000000000000000000000001001100000000000000000000000000001011000000000000000000000000000011011000000000000000000000000110010000000000000000000000000000001100100000000000000000000000000100000000000000000000000000000000```

- machine

```

DEBUG:root:{INSTRUCT: 1, IP: 1, ADDR: 1, ACC: 0, Z: False, C: False, N: False,} JMP 1 
DEBUG:root:{INSTRUCT: 2, IP: 1, ADDR: 2, ACC: 116, Z: False, C: False, N: False,} IN 1 
DEBUG:root:{INSTRUCT: 3, IP: 0, ADDR: 3, ACC: 116, Z: False, C: False, N: False,} CMP 0 
DEBUG:root:{INSTRUCT: 4, IP: 6, ADDR: 4, ACC: 116, Z: False, C: False, N: False,} JE 0 
DEBUG:root:{INSTRUCT: 5, IP: 6, ADDR: 5, ACC: 116, Z: False, C: False, N: False,} OUT 0 
DEBUG:root:{INSTRUCT: 6, IP: 1, ADDR: 1, ACC: 116, Z: False, C: False, N: False,} JMP 12288 
DEBUG:root:{INSTRUCT: 7, IP: 1, ADDR: 2, ACC: 101, Z: False, C: False, N: False,} IN 12288 
DEBUG:root:{INSTRUCT: 8, IP: 0, ADDR: 3, ACC: 101, Z: False, C: False, N: False,} CMP 0 
DEBUG:root:{INSTRUCT: 9, IP: 6, ADDR: 4, ACC: 101, Z: False, C: False, N: False,} JE 0 
DEBUG:root:{INSTRUCT: 10, IP: 6, ADDR: 5, ACC: 101, Z: False, C: False, N: False,} OUT 0 
DEBUG:root:{INSTRUCT: 11, IP: 1, ADDR: 1, ACC: 101, Z: False, C: False, N: False,} JMP 12288 
DEBUG:root:{INSTRUCT: 12, IP: 1, ADDR: 2, ACC: 115, Z: False, C: False, N: False,} IN 12288 
DEBUG:root:{INSTRUCT: 13, IP: 0, ADDR: 3, ACC: 115, Z: False, C: False, N: False,} CMP 0 
DEBUG:root:{INSTRUCT: 14, IP: 6, ADDR: 4, ACC: 115, Z: False, C: False, N: False,} JE 0 
DEBUG:root:{INSTRUCT: 15, IP: 6, ADDR: 5, ACC: 115, Z: False, C: False, N: False,} OUT 0 
DEBUG:root:{INSTRUCT: 16, IP: 1, ADDR: 1, ACC: 115, Z: False, C: False, N: False,} JMP 12288 
DEBUG:root:{INSTRUCT: 17, IP: 1, ADDR: 2, ACC: 116, Z: False, C: False, N: False,} IN 12288 
DEBUG:root:{INSTRUCT: 18, IP: 0, ADDR: 3, ACC: 116, Z: False, C: False, N: False,} CMP 0 
DEBUG:root:{INSTRUCT: 19, IP: 6, ADDR: 4, ACC: 116, Z: False, C: False, N: False,} JE 0 
DEBUG:root:{INSTRUCT: 20, IP: 6, ADDR: 5, ACC: 116, Z: False, C: False, N: False,} OUT 0 
DEBUG:root:{INSTRUCT: 21, IP: 1, ADDR: 1, ACC: 116, Z: False, C: False, N: False,} JMP 12288 
DEBUG:root:{INSTRUCT: 22, IP: 1, ADDR: 2, ACC: 32, Z: False, C: False, N: False,} IN 12288 
DEBUG:root:{INSTRUCT: 23, IP: 0, ADDR: 3, ACC: 32, Z: False, C: False, N: False,} CMP 0 
DEBUG:root:{INSTRUCT: 24, IP: 6, ADDR: 4, ACC: 32, Z: False, C: False, N: False,} JE 0 
DEBUG:root:{INSTRUCT: 25, IP: 6, ADDR: 5, ACC: 32, Z: False, C: False, N: False,} OUT 0 
DEBUG:root:{INSTRUCT: 26, IP: 1, ADDR: 1, ACC: 32, Z: False, C: False, N: False,} JMP 12288 
DEBUG:root:{INSTRUCT: 27, IP: 1, ADDR: 2, ACC: 99, Z: False, C: False, N: False,} IN 12288 
DEBUG:root:{INSTRUCT: 28, IP: 0, ADDR: 3, ACC: 99, Z: False, C: False, N: False,} CMP 0 
DEBUG:root:{INSTRUCT: 29, IP: 6, ADDR: 4, ACC: 99, Z: False, C: False, N: False,} JE 0 
DEBUG:root:{INSTRUCT: 30, IP: 6, ADDR: 5, ACC: 99, Z: False, C: False, N: False,} OUT 0 
DEBUG:root:{INSTRUCT: 31, IP: 1, ADDR: 1, ACC: 99, Z: False, C: False, N: False,} JMP 12288 
DEBUG:root:{INSTRUCT: 32, IP: 1, ADDR: 2, ACC: 97, Z: False, C: False, N: False,} IN 12288 
DEBUG:root:{INSTRUCT: 33, IP: 0, ADDR: 3, ACC: 97, Z: False, C: False, N: False,} CMP 0 
DEBUG:root:{INSTRUCT: 34, IP: 6, ADDR: 4, ACC: 97, Z: False, C: False, N: False,} JE 0 
DEBUG:root:{INSTRUCT: 35, IP: 6, ADDR: 5, ACC: 97, Z: False, C: False, N: False,} OUT 0 
DEBUG:root:{INSTRUCT: 36, IP: 1, ADDR: 1, ACC: 97, Z: False, C: False, N: False,} JMP 12288 
DEBUG:root:{INSTRUCT: 37, IP: 1, ADDR: 2, ACC: 116, Z: False, C: False, N: False,} IN 12288 
DEBUG:root:{INSTRUCT: 38, IP: 0, ADDR: 3, ACC: 116, Z: False, C: False, N: False,} CMP 0 
DEBUG:root:{INSTRUCT: 39, IP: 6, ADDR: 4, ACC: 116, Z: False, C: False, N: False,} JE 0 
DEBUG:root:{INSTRUCT: 40, IP: 6, ADDR: 5, ACC: 116, Z: False, C: False, N: False,} OUT 0 
DEBUG:root:{INSTRUCT: 41, IP: 1, ADDR: 1, ACC: 116, Z: False, C: False, N: False,} JMP 12288 
DEBUG:root:{INSTRUCT: 42, IP: 1, ADDR: 2, ACC: 0, Z: False, C: False, N: False,} IN 12288 
DEBUG:root:{INSTRUCT: 43, IP: 0, ADDR: 3, ACC: 0, Z: True, C: False, N: False,} CMP 0 
DEBUG:root:{INSTRUCT: 44, IP: 6, ADDR: 6, ACC: 0, Z: True, C: False, N: False,} JE 0 
INFO:root:output: ['t', 'e', 's', 't', ' ', 'c', 'a', 't']
Output:
test cat
instr_counter: 45 ticks: 134

Process finished with exit code 0


```


| ФИО             | алг.  | LoC  | code байт | code инстр. | инстр. | такт. |
|-----------------|-------|------|-----------|-------------|--------|-------|
| Оскилко М. В.   | hello | 37   | 9         | 24          | 26     | 77    | 
| Оскилко М. В.   | cat   | 9    | ----      | 6           | 45     | 134   | 
| Оскилко М. В.   | prob1 | 40   |           | 33          | 33     | 112   | 







