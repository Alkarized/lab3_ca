from enum import Enum, IntEnum

MASK_OPERAND: int = 0b00000111111111111111111111111111
MASK_ADDR: int = 0b00001000000000000000000000000000
MASK_COMMAND: int = 0b11110000000000000000000000000000
WORD_LENGTH: int = 32
OPERAND_LENGTH: int = 27
ADDRESS_LIMIT = 2 ** 30


class Operation:
    def __init__(self, code: int, need_argument: bool):
        self.code: int = code
        self.need_argument: bool = need_argument

    def bore(self):
        print(self.code)

    def wiggle(self):
        print(self.need_argument)


class Command(Operation, Enum):
    HALT = 0b00000000000000000000000000000000, False
    INC = 0b00010000000000000000000000000000, False
    DEC = 0b00100000000000000000000000000000, False
    IN = 0b00110000000000000000000000000000, False
    OUT = 0b01000000000000000000000000000000, False
    LD = 0b01010000000000000000000000000000, True
    SV = 0b01100000000000000000000000000000, True
    ADD = 0b01110000000000000000000000000000, True
    SUB = 0b10000000000000000000000000000000, True
    MUL = 0b10010000000000000000000000000000, True
    DIV = 0b10100000000000000000000000000000, True
    CMP = 0b10110000000000000000000000000000, True
    JMP = 0b11000000000000000000000000000000, True
    JE = 0b11010000000000000000000000000000, True
    JGE = 0b11100000000000000000000000000000, True
    JC = 0b11110000000000000000000000000000, True


class AddrMode(IntEnum):
    STRAIGHT = 0b00000000000000000000000000000000
    ABSOLUTE = 0b00001000000000000000000000000000


addrmode_mapper = {
    0b00000000000000000000000000000000: AddrMode.STRAIGHT,
    0b00001000000000000000000000000000: AddrMode.ABSOLUTE
}

command_mapper = {
    0b00000000000000000000000000000000: Command.HALT,
    0b00010000000000000000000000000000: Command.INC,
    0b00100000000000000000000000000000: Command.DEC,
    0b00110000000000000000000000000000: Command.IN,
    0b01000000000000000000000000000000: Command.OUT,
    0b01010000000000000000000000000000: Command.LD,
    0b01100000000000000000000000000000: Command.SV,
    0b01110000000000000000000000000000: Command.ADD,
    0b10000000000000000000000000000000: Command.SUB,
    0b10010000000000000000000000000000: Command.MUL,
    0b10100000000000000000000000000000: Command.DIV,
    0b10110000000000000000000000000000: Command.CMP,
    0b11000000000000000000000000000000: Command.JMP,
    0b11010000000000000000000000000000: Command.JE,
    0b11100000000000000000000000000000: Command.JGE,
    0b11110000000000000000000000000000: Command.JC
}


def get_bin(num, bits):
    """Получить двоичную bits-разрядную запись числа num"""
    return format(int(num), 'b').zfill(bits)


def write_code(filename: str, code) -> None:
    """Записать машинный код в файл."""

    with open(filename, "w", encoding="utf-8") as file:
        for code_line in code:
            file.write(get_bin(code_line, WORD_LENGTH))


def read_code(filename: str) -> list[int]:
    """Прочесть машинный код из файла."""

    code: list[int] = []

    alldata: str

    with open(filename, encoding="utf-8") as file:
        alldata = file.read()

    for i in range(int(len(alldata) / WORD_LENGTH)):
        data_bits = alldata[:WORD_LENGTH]
        alldata = alldata[WORD_LENGTH:]
        code_int = int(data_bits, 2)
        code.append(code_int)

    return code
