import logging
import copy

from isa import ADDRESS_LIMIT, read_code, MASK_COMMAND, MASK_OPERAND, MASK_ADDR, command_mapper, AddrMode, Command, \
    WORD_LENGTH

logging.getLogger().setLevel(logging.DEBUG)


class Halt(Exception):
    def __init__(self, message="Точка остановки!"):
        self.message = message
        super().__init__(self.message)


class ControlUnit:

    def __init__(self, code: list[int], input_token: list[int], instr_limit: int):
        self.code: list[int] = code
        self.input_token = input_token
        self.tick: int = 0
        self.instr_counter: int = 0
        self.instr_limit: int = instr_limit
        self.address: int = 0  # IP
        self.accumulator: int = 0  # AC
        self.output: list = []
        self.command: int = 0  # CR
        self.command_code: int = 0
        self.command_argument: int = 0  # OP
        self.command_addr_mode: int = 0
        self.neg_flag: bool = False  # NF
        self.zero_flag: int = False  # ZF
        self.carry_flag: int = False  # CF
        self.command_operand_address: int = 0

    def set_nz_flags(self):
        self.set_neg_flag()
        self.set_zero_flag()

    def set_neg_flag(self):
        return not self.accumulator >= 0

    def set_zero_flag(self):
        return self.accumulator == 0

    def increment_tick(self):
        self.tick += 1

    def increment_address(self) -> None:
        self.address += 1
        assert self.address <= len(self.code), "Программа вышла за свои пределы!"

    def get_command(self, address):
        assert self.code[address].bit_length() <= WORD_LENGTH and address < len(
            self.code), "Проблема с загрузкой команды!"

        self.command = self.code[address]

    def get_command_code(self, address):
        assert self.code[address].bit_length() <= WORD_LENGTH and address < len(
            self.code), "Проблема с загрузкой кода команды!"
        self.command_code = self.code[address] & MASK_COMMAND

    def get_command_mode(self, address):
        assert self.code[address].bit_length() <= WORD_LENGTH and address < len(
            self.code), "Проблема с загрузкой кода мод адреса!"

        self.command_addr_mode = self.code[address] & MASK_ADDR

    def execute_command(self):
        self.get_command_code(self.address)

        if command_mapper[self.command_code].need_argument:
            self.get_command_mode(self.address)

            self.increment_tick()

            match self.command_addr_mode:

                case AddrMode.STRAIGHT:
                    self.command_argument = self.code[self.address] & MASK_OPERAND
                    self.command_operand_address = self.command_argument

                case AddrMode.ABSOLUTE:
                    self.increment_tick()
                    self.command_operand_address = self.code[self.address] & MASK_OPERAND  # addres
                    self.command_argument = self.code[self.command_operand_address]

        self.increment_tick()
        self.increment_address()

        match command_mapper[self.command_code]:
            case Command.HALT:
                raise Halt

            case Command.INC:
                res: int = self.accumulator + 1
                self.carry_flag = False
                if res > 2147483647:
                    self.accumulator = -2147483648 + (res - 2147483647)
                    self.carry_flag = True
                else:
                    self.accumulator += 1
                self.set_nz_flags()

            case Command.DEC:
                res: int = self.accumulator - 1
                self.carry_flag = False
                if res < -2147483648:
                    self.accumulator = 2147483647 - (res + 2147483648)
                    self.carry_flag = True
                else:
                    self.accumulator -= 1
                self.set_nz_flags()

            case Command.ADD:
                res: int = self.accumulator + self.command_argument
                self.carry_flag = False
                if res > 2147483647:
                    self.accumulator = -2147483648 + (res - 2147483647)
                    self.carry_flag = True
                else:
                    self.accumulator += self.command_argument
                self.set_nz_flags()

            case Command.SUB:
                res: int = self.accumulator - self.command_argument
                self.carry_flag = False
                if res < -2147483648:
                    self.accumulator = 2147483647 - (res + 2147483648)
                    self.carry_flag = True
                else:
                    self.accumulator -= self.command_argument
                self.set_nz_flags()

            case Command.MUL:
                res: int = self.accumulator * self.command_argument
                self.carry_flag = False
                while res > 2147483647:
                    res = -2147483648 + (res - 2147483647)
                    self.carry_flag = True

                self.accumulator = res
                self.set_nz_flags()

            case Command.DIV:
                self.accumulator = int(self.accumulator / self.command_argument)
                if self.accumulator % self.command_argument != 0:
                    self.carry_flag = False
                else:
                    self.carry_flag = True
                self.set_nz_flags()

            case Command.LD:
                self.accumulator = copy.copy(self.command_argument)
                self.set_nz_flags()

            case Command.SV:
                self.code[self.command_operand_address] = copy.copy(self.accumulator)

            case Command.CMP:
                res: int = self.accumulator - self.command_argument
                self.carry_flag = False
                if res < -32768:
                    res = 32767 - (res + 32768)
                    self.carry_flag = True

                if res == 0:
                    self.zero_flag = True
                elif res < 0:
                    self.neg_flag = True

            case Command.JMP:
                self.address = self.command_operand_address

            case Command.JE:
                if self.zero_flag:
                    self.address = self.command_operand_address

            case Command.JGE:
                if not self.neg_flag:
                    self.address = self.command_operand_address

            case Command.JC:
                if self.carry_flag:
                    self.address = self.command_operand_address

            case Command.IN:
                assert len(self.input_token) > 0, "Ничего в вводе нет!"
                self.accumulator = self.input_token[0]
                del self.input_token[0]

            case Command.OUT:

                if ord('A') < self.accumulator < ord('z') or ord(' ') == self.accumulator or ord(
                        '!') == self.accumulator:
                    self.output.append(chr(self.accumulator))
                else:
                    self.output.append(str(self.accumulator))

    def __repr__(self):
        state = "{{INSTRUCT: {}, IP: {}, ADDR: {}, ACC: {}, Z: {}, C: {}, N: {},}}".format(
            self.instr_counter,
            self.command_operand_address,
            self.address,
            self.accumulator,
            self.zero_flag,
            self.carry_flag,
            self.neg_flag,
        )

        opcode = command_mapper[self.command_code].name
        arg = self.command_argument
        # mode = addrmode_mapper[self.command_addr_mode].name
        action = "{} {} ".format(opcode, arg)

        return "{} {}".format(state, action)


def work(code: list[int], input_token: list[int], instr_limit: int):
    control_unit: ControlUnit = ControlUnit(code, input_token, instr_limit)

    try:
        while True:
            control_unit.increment_tick()
            control_unit.instr_counter += 1
            assert control_unit.instr_counter < control_unit.instr_limit, "Достигнут Лимит инструкция"
            control_unit.get_command(control_unit.address)
            control_unit.execute_command()
            logging.debug('%s', control_unit)
    except Halt:
        pass

    logging.info('output: %s', control_unit.output)

    return ''.join(control_unit.output), control_unit.instr_counter, control_unit.tick


def main(args):
    assert len(args) == 2, "Wrong arguments: machine.py <code_file> <input_file>"
    code_file, input_file = args

    input_token: list[int] = []
    code = read_code(code_file)

    with open(input_file, encoding="utf-8") as file:
        input_text: str = file.read()
        for char in input_text:
            input_token.append(ord(char))

    input_token.append(0)

    assert len(code) < ADDRESS_LIMIT, "Не хватает памяти!"

    output, instr_counter, ticks = work(code, input_token, 1000)

    print("Output:")
    print(output)
    print("instr_counter:", instr_counter, "ticks:", ticks)
    return output


if __name__ == '__main__':
    # main(sys.argv[1:])
    # print(ord('A'))
    main(["tests/hello_test/result", "tests/hello_test/input.txt"])
