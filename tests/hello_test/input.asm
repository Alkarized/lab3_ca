.data
    H h
    E e
    L l
    O o
    space 32
    W w
    R r
    D d
    ! !
.code
    .start:
        LD ^H
        OUT
        LD ^E
        OUT
        LD ^L
        OUT
        LD ^L
        OUT
        LD ^O
        OUT
        LD ^space
        OUT
        LD ^W
        OUT
        LD ^O
        OUT
        LD ^R
        OUT
        LD ^L
        OUT
        LD ^D
        OUT
        LD ^!
        OUT
        HALT
